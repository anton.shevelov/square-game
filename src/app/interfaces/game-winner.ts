export interface GameWinner {
  id?: number;
  winner: string;
  date: string;
}
