import { Injectable, ErrorHandler } from '@angular/core';
import { Cell } from 'src/app/classes/cell';
import { GenericSubject } from 'src/app/classes/generic-subject';
import {
  CELL_EMPTY,
  CELL_COMPUTER,
  CELL_AVAILABLE,
  CELL_USER,
} from '@const/cell-states';
import * as moment from 'moment';
import { GameMode } from '@interface/game-mode';
import { LeaderBordService } from './leader-bord.service';
import { TIME_FORMAT } from '@const/time-format';
@Injectable({
  providedIn: 'root',
})
export class GameService implements ErrorHandler {
  public board: GenericSubject<Cell[]> = new GenericSubject<Cell[]>([]);
  public winner: GenericSubject<string> = new GenericSubject<string>('');
  public boardLength = 0;
  private userName = '';
  private userCount = 0;
  private computerCount = 0;
  private moveDelay = 0;
  private activeCellID = null;
  private timeout = null;

  constructor(private lbs: LeaderBordService) {}

  startGame(gameMode: GameMode, userName: string): void {
    this.userName = userName;
    this.winner.value = '';
    this.stopGame();
    const { field, delay } = gameMode;
    this.moveDelay = delay;
    this.computerCount = 0;
    this.userCount = 0;
    this.generateBoard(field);
    this.activateRandomCell();
  }
  generateBoard(field: number) {
    this.boardLength = field * field;
    this.board.value = [...Array(this.boardLength).keys()].map(
      (cell, index) => {
        return new Cell({
          id: index,
          status: CELL_EMPTY,
        });
      }
    );
  }
  activateRandomCell(): void {
    const notAvailableCells = this.board.value
      .filter((cell: Cell) => {
        return cell.status !== CELL_EMPTY;
      })
      .map((cell: Cell) => {
        return cell.id;
      });
    while (
      this.activeCellID === null ||
      notAvailableCells.includes(this.activeCellID)
    ) {
      this.activeCellID = Math.round(Math.random() * (this.boardLength - 1));
    }
    this.changeCellStatus(this.activeCellID, CELL_AVAILABLE);
    this.computerMove();
  }

  computerMove(): void {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.changeCellStatus(this.activeCellID, CELL_COMPUTER);
      this.activateRandomCell();
    }, this.moveDelay);
  }

  userMove(index: number): void {
    clearTimeout(this.timeout);
    this.changeCellStatus(index, CELL_USER);
    if (this.activeCellID) {
      this.activateRandomCell();
    }
  }

  changeCellStatus(index: number, status: number): void {
    this.board.value[index].status = status;
    this.calculateScore(status);
  }

  calculateScore(status: number) {
    switch (status) {
      case CELL_COMPUTER:
        this.computerCount++;
        break;
      case CELL_USER:
        this.userCount++;
        break;
    }
    if (
      this.userCount > this.boardLength / 2 ||
      this.computerCount > this.boardLength / 2
    ) {
      this.stopGame();
      if (this.userCount > this.computerCount) {
        this.winner.value = this.userName;
      } else {
        this.winner.value = 'Computer';
      }
      this.lbs.sendWinner(this.winner.value, moment().format(TIME_FORMAT));
    }
  }
  stopGame(): void {
    clearTimeout(this.timeout);
    this.moveDelay = 0;
    this.activeCellID = null;
  }

  handleError(error: any): void {
    console.log('error', error);
  }
}
