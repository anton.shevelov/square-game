import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { GameService } from '@module/game/services/game.service';
import { Cell } from 'src/app/classes/cell';
import { Subject } from 'rxjs';
import {
  CELL_EMPTY,
  CELL_USER,
  CELL_COMPUTER,
  CELL_AVAILABLE,
} from '@const/cell-states';
import { GenericSubject } from 'src/app/classes/generic-subject';

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss'],
})
export class GameBoardComponent implements OnInit {
  public board: GenericSubject<Cell[]> = new GenericSubject<Cell[]>([]);
  public cellColors = {
    [CELL_EMPTY]: 'white',
    [CELL_USER]: 'green',
    [CELL_COMPUTER]: 'red',
    [CELL_AVAILABLE]: 'blue',
  };

  constructor(private gs: GameService, private host: ElementRef) {}

  ngOnInit(): void {
    this.board = this.gs.board;
  }

  trackByIndex(index: number, cell: Cell) {
    return cell.id;
  }

  calculateSize() {
    const containerWidth = this.host.nativeElement.clientWidth;
    const size = Math.sqrt(this.board.value.length);
    return containerWidth / size;
  }

  pickUpCellByUser(cell, index): void {
    if (cell.status !== CELL_AVAILABLE) {
      return;
    }
    this.gs.userMove(index);
  }
}
