import { Component, OnInit, OnDestroy } from '@angular/core';
import { LeaderBordService } from '@module/game/services/leader-bord.service';
import { GameWinner } from '@interface/game-winner';
import { GenericSubject } from 'src/app/classes/generic-subject';

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss'],
})
export class LeaderBoardComponent implements OnInit {
  public winners: GenericSubject<GameWinner[]> = new GenericSubject<
    GameWinner[]
  >([]);

  constructor(private lbs: LeaderBordService) {
    this.lbs.retrieveWinners();
  }

  ngOnInit(): void {
    this.winners = this.lbs.winners;
  }
}
