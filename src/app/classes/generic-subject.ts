import { BehaviorSubject } from 'rxjs';

export class GenericSubject<T> {
  private readonly $value: BehaviorSubject<T> = new BehaviorSubject(null);
  constructor(value: T) {
    this.value = value;
  }
  get value(): T {
    return this.$value.value;
  }

  set value(nextValue: T) {
    this.$value.next(nextValue);
  }

  public async(): BehaviorSubject<T> {
    return this.$value;
  }
}
